/**
 * 
 */
package uk.co.three.rebus.three_rebus.core.services;

import java.util.ArrayList;
import java.util.Map;


public interface AzureProductReadService {
	
	public Map<String, ArrayList> initiateRead(String value);

}
