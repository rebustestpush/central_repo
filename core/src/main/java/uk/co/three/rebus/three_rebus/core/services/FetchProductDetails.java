package uk.co.three.rebus.three_rebus.core.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(label="Product Details",description="Fetches Product Details",immediate=true)
@Service(value=FetchProductDetails.class)
public class FetchProductDetails {
	Logger LOGGER = LoggerFactory.getLogger(FetchProductDetails.class);
	
	@Reference
	ResourceResolverFactory resourceFactory;
	
	public ArrayList getProductDetails(String id){
		
		Map<String, Object> serviceParams = new HashMap<String, Object>();
		serviceParams.put(ResourceResolverFactory.SUBSERVICE, "readnode");
		ArrayList<String> productDetails = new ArrayList<String>(); 
		
		LOGGER.info("Request Param:"+id);
		String queryStatement = "SELECT * FROM [nt:base] AS nodes WHERE CONTAINS(nodes.masterCatalogueReference,'"+id+"')";
		LOGGER.info("Query String:"+queryStatement);
		try {
			ResourceResolver resourceResolver = resourceFactory.getServiceResourceResolver(serviceParams);
			Session session = resourceResolver.adaptTo(Session.class);
			QueryManager queryManager =  session.getWorkspace().getQueryManager();
			Query query = queryManager.createQuery(queryStatement, "JCR-SQL2");
			QueryResult result = query.execute();
			
			NodeIterator nodeItr = result.getNodes();
			LOGGER.info("nodeItr"+nodeItr.hasNext());
			while(nodeItr.hasNext()){
				Node node = nodeItr.nextNode();
				LOGGER.info("NODE"+node.getName());
				PropertyIterator propertyItr= node.getProperties();
				while(propertyItr.hasNext()){
					productDetails.add(propertyItr.nextProperty().getString());
					
				}LOGGER.info("LIST"+productDetails);
			}
			
		} catch (LoginException e) {
			LOGGER.info("Login Exception:"+e);
		} catch (RepositoryException e) {
			LOGGER.info("RepositoryException:"+e);
		}
		
		return productDetails;
	}

}
