package uk.co.three.rebus.three_rebus.core.listeners;

import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(immediate=true)
@Service
public class PropertyListner implements EventListener {
	
	private final Logger LOGGER = LoggerFactory.getLogger(PropertyListner.class);
	
	@Reference
	private SlingRepository repository;
	
	private Session session;
	private ObservationManager observationManager;
	public void run(){
		LOGGER.info("run");
	}
	@Activate
	protected void activate(ComponentContext context) throws Exception {
		LOGGER.info("********Inside Activ");
		Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
		observationManager = session.getWorkspace().getObservationManager();
		
		observationManager.addEventListener(this, Event.PROPERTY_ADDED | Event.PROPERTY_CHANGED, "/", true, null, 
				null, true);
		LOGGER.info("********added JCR event listener");
	}
	@Deactivate
	protected void deactivate(ComponentContext componentContext) {
		try {
			if (observationManager != null) {
				observationManager.removeEventListener(this);
				LOGGER.info("********removed JCR event listener");
			}
		}
		catch (RepositoryException re) {
			LOGGER.error("********error removing the JCR event listener", re);
		}
		finally {
			if (session != null) {
				session.logout();
				session = null;
			}
		}
	}
	
	public void onEvent(EventIterator it) {
		while (it.hasNext()) {
			Event event = it.nextEvent();
			try {
				LOGGER.info("********new property event: {}", event.getPath());
				Property changedProperty = session.getProperty(event.getPath());
				
				if (changedProperty.getName().equalsIgnoreCase("jcr:title")
						&& !changedProperty.getString().endsWith("!")) {
					changedProperty.setValue(changedProperty.getString() + "!");
					session.save();
				}
			}
			catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}		
	}
}
