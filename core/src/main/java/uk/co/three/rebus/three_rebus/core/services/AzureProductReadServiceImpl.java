package uk.co.three.rebus.three_rebus.core.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.print.attribute.standard.PrinterMessageFromOperator;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(label="Service Implementation for reading Product Nodes",
description="Implementation for reading the product nodes and displaying it",
metatype=true,immediate=true)
@Service(value=AzureProductReadServiceImpl.class)
public class AzureProductReadServiceImpl implements AzureProductReadService {
		
	Logger LOGGER = LoggerFactory.getLogger(AzureProductReadServiceImpl.class);
	
	@Reference
	private ResourceResolverFactory resourceFactory;
	
	
	//private static String productPath = "/content/three_rebus/products";
	private static String productPath ="/content/three_UK/products";
	
	
	//private Map<String,ArrayList> readServiceMethod(ResourceResolver resourceResolver, String productPath, String queryStatement2) {
	private Map<String,ArrayList> readServiceMethod(ResourceResolver resourceResolver,String queryStatement) {
		
		Map<String, Object> serviceParams = new HashMap<String, Object>();
        serviceParams.put(ResourceResolverFactory.SUBSERVICE, "readnode");
        
       
        //Map<String, ArrayList> propertyMap = new HashMap<String, ArrayList>();
        Map<String, ArrayList> propertyMap = new LinkedHashMap<String, ArrayList>();
       // String queryStatement = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(["+productPath+"])";
        LOGGER.info("Reading Nodes from path:"+productPath);
        
         try {
        	 
        	 /*
        	  * establish a facaory interface to get the JCRQueryConn Manager
        	  * from manager.getServiceFactory().getService('prodServ')
        	  * localProductObj.invokeService(queryManager.query()) 
        	  */
        	 
			resourceResolver = resourceFactory.getServiceResourceResolver(serviceParams);
			Session session = resourceResolver.adaptTo(Session.class);
			LOGGER.info("ServiceParam:"+serviceParams+"ResourceResolver:"+resourceResolver);
			QueryManager queryManger = session.getWorkspace().getQueryManager();
			//queryStatement = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(["+productPath+"])";
			LOGGER.info("Query Statement:"+queryStatement);
			Query query = queryManger.createQuery(queryStatement, "JCR-SQL2");
			
			QueryResult queryResult = query.execute();
			
			NodeIterator nodeItr = queryResult.getNodes();
			LOGGER.info("NodeItr"+nodeItr);
			while(nodeItr.hasNext()){
				Node node = nodeItr.nextNode();
				LOGGER.info("Node"+node.getProperty("masterCatalogueReference").getString());
				 ArrayList<String> propertyList = new ArrayList<String>();
				propertyList.add(node.getProperty("masterCatalogueReference").getString());
				propertyList.add(node.getProperty("monthly-rental").getString());
				propertyList.add(node.getProperty("payment-type").getString());
				propertyList.add(node.getProperty("price").getString());
				propertyList.add(node.getProperty("product-description").getString());
				propertyList.add(node.getProperty("product-name").getString());
				propertyList.add(node.getProperty("owner").getString());
				propertyList.add(node.getProperty("upfront").getString());
				//propertyList.add(node.getProperty("sfdcCatalogueReference").getString());
				
				propertyMap.put(node.getProperty("masterCatalogueReference").getString(), propertyList);
				
				
				LOGGER.info("PropertyMapBefore:"+propertyMap);
				
				LOGGER.info("PropertyList:"+propertyList.size());
			}
			LOGGER.info("PropertyMapAfter:"+propertyMap);
		} catch (LoginException e) {
			LOGGER.info("Login Exception"+e);
		}catch (RepositoryException e) {
			LOGGER.info("Repository Exception"+e);
		}
		//return propertyList; 
         return propertyMap;
	}


	public Map<String, ArrayList> initiateRead(String queryStatement) {
		Map<String, Object> serviceParams = new HashMap<String, Object>();
        serviceParams.put(ResourceResolverFactory.SUBSERVICE, "readnode");
        Map<String, ArrayList> propertyMap1 = new HashMap<String, ArrayList>();
        
        ArrayList<String> propertyList = new ArrayList<String>();
        ResourceResolver resourceResolver;
		try {
			resourceResolver = resourceFactory.getServiceResourceResolver(serviceParams);
			//propertyMap1 = readServiceMethod(resourceResolver,productPath,queryStatement);
			propertyMap1 = readServiceMethod(resourceResolver,queryStatement);
			//propertyList = (ArrayList<String>) readServiceMethod(resourceResolver,productPath);
		} catch (LoginException e) {
			
			e.printStackTrace();
		}
		LOGGER.info("Intiate Method"+propertyList);
		//return propertyList;
		return propertyMap1;
	}

}
