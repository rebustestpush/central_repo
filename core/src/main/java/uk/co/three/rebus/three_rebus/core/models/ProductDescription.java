package uk.co.three.rebus.three_rebus.core.models;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;

import org.apache.sling.models.annotations.Model;

@Model(adaptables = SlingHttpServletRequest.class)
public class ProductDescription {
	@Inject private SlingHttpServletRequest request;
	
	private ArrayList<String> productList = new ArrayList<String>();
	private String check;
	@PostConstruct
	public void activate(){
		productList = (ArrayList<String>) request.getAttribute("responseProductList");
		check = "In Model";
	}
	
	public ArrayList<String> getProductInfo(){
		return productList;
	}
	public String getString(){
		return check;
	}
}
